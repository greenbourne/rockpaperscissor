import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm") version "1.4.10"
}

group = "de.cbay"
version = "1.0-0"

task("execute", JavaExec::class) {
    main = "de.cbay.ApplicationKt"
    classpath = sourceSets["main"].runtimeClasspath
}

repositories {
    mavenCentral()
}
dependencies {
    testImplementation(kotlin("test-testng"))
}
tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}