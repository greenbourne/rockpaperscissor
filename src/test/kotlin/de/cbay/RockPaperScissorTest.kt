package de.cbay

import de.cbay.GameResult.*
import de.cbay.RockPaperScissor.*
import org.testng.Assert.*
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

/**
 * Testing [RockPaperScissor].
 *
 * @author Christian Bay (christian.bay@posteo.net)
 */
class RockPaperScissorTest {

    @DataProvider(name = DATA_FOR_EVALUATE_GAME_ROUND)
    private fun createDataForMakePlay(): Array<Array<Any>> =
        arrayOf(
            arrayOf(
                ROCK,
                SCISSOR,
                WIN
            ),
            arrayOf(
                ROCK,
                PAPER,
                LOSS
            ),
            arrayOf(
                ROCK,
                ROCK,
                DRAW
            ),
            arrayOf(
                PAPER,
                ROCK,
                WIN
            ),
            arrayOf(
                PAPER,
                SCISSOR,
                LOSS
            ),
            arrayOf(
                PAPER,
                PAPER,
                DRAW
            ),
            arrayOf(
                SCISSOR,
                SCISSOR,
                DRAW
            ),
            arrayOf(
                SCISSOR,
                ROCK,
                LOSS
            ),
            arrayOf(
                SCISSOR,
                PAPER,
                WIN
            ),
        )

    @Test(dataProvider = DATA_FOR_EVALUATE_GAME_ROUND)
    fun testEvaluateGameRound(
        play1: RockPaperScissor,
        play2: RockPaperScissor,
        expectedResult: GameResult
    ) {
        val gameResult = RockPaperScissor.evaluateGameRound(play1, play2)

        assertEquals(gameResult, expectedResult)
    }

    companion object {
        private const val DATA_FOR_EVALUATE_GAME_ROUND = "DATA_FOR_EVALUATE_GAME_ROUND"
    }
}