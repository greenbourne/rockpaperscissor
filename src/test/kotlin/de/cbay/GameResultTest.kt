package de.cbay

import de.cbay.GameResult.*
import org.testng.annotations.Test

import org.testng.Assert.*

/**
 * Testing [GameResult].
 *
 * @author Christian Bay (christian.bay@posteo.net)
 */
class GameResultTest {

    @Test
    fun testInverse() {
        // Test inverting WIN
        assertEquals(LOSS, WIN.invert())
        // Test inverting LOSS
        assertEquals(WIN, LOSS.invert())
        // Test inverting DRAW
        assertEquals(DRAW, DRAW.invert())
    }
}