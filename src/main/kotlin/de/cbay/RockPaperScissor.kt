package de.cbay

/**
 * Possible plays for Rock-Paper-Scissor.
 *
 * @author Christian Bay (christian.bay@posteo.net)
 */
enum class RockPaperScissor {
    ROCK,
    PAPER,
    SCISSOR;

    private fun isSuperior(other: RockPaperScissor) : Boolean =
       when(this){
            ROCK -> other == SCISSOR
            PAPER -> other == ROCK
            SCISSOR -> other == PAPER
    }

    companion object {
        fun evaluateGameRound(first: RockPaperScissor, snd: RockPaperScissor) : GameResult {
            if (first == snd){
                return GameResult.DRAW
            }
            if (first.isSuperior(snd)){
                return GameResult.WIN
            }
            return GameResult.LOSS
        }
    }
}
