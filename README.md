# Notes

## Info
Just a simple RockPaperScissor simulator written in Kotlin.

## Usage

Choose one of the options mentioned below to run the program:

	1. Use gradle in the command line via `./gradlew :execute`
	2. Use JetBrains Idea [https://www.jetbrains.com/idea/] and run the application there.

# License

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.
